from django import template
# from app1.models import Category
import requests

register = template.Library()


@register.filter
def custom_lower(value):
    print(value)
    return value[:8].lower()


@register.filter
def set_class(id_iteration):
    return 'alert-success' if id_iteration % 2 == 0 else 'alert-danger'
