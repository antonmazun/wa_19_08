from django.urls import path, include, re_path
from . import views, auth_views

app_name = 'client'

urlpatterns = [
    path('show-book/<int:pk>', views.book_info, name='book-info'),
    path('filters', views.filters, name='filters'),
    path('html', views.html_example),
    path('category-<int:pk>', views.filter_by_category),
    path('cabinet', views.cabinet, name='cabinet'),
    path('return-json', views.return_json),
    # path('login/', auth_views.login_, name='login'),
    # path('logout', auth_views.logout_, name='logout'),
    path('calculator', views.calculator),
    path('demo-model-form', views.demo_model_form, name='demo-model-form'),
    path('privatbank-api', views.api_privatbank, name='privatbank-api'),
    path('my_articles', views.my_articles, name='my_articles'),
    path('write_post', views.write_post, name='write_post'),
    path('delete_article-<int:pk>', views.delete_article, name='delete_article'),
    path('articles-user', views.articles_user, name='articles-user'),
    path('article/<int:pk>', views.article, name='article'),
    path('add-comment/<int:article_id>', views.add_comment, name='add_comment'),
    path('update_article/<int:pk>',
         views.update_article,
         name='update_article'),
]

auth_urls = [
    path('login-form/', auth_views.login_, name='login'),
    path('rgister-form/', auth_views.register_, name='register'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.activate, name='activate'),
    path('logout', auth_views.logout_, name='logout'),
]

urlpatterns += auth_urls
